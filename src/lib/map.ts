export default function <T, K>(
  a: T[][],
  callback: (value: T, x: number, y: number) => K
) {
  return a.map((b, y) => b.map((v, x) => callback(v, x, y)));
}
