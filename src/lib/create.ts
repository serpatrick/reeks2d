export default function <T>(width: number, height: number, value: T) {
  return Array(height)
    .fill(value)
    .map(() => Array(width).fill(value)) as T[][];
}
