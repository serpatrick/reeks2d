import get from './get.js';
import height from './height.js';
import isOutOfBounds from './isOutOfBounds.js';
import width from './width.js';

export default function <T>(
  a: T[][],
  callback: (value: T, x: number, y: number) => void,
  stepX = 1,
  stepY = 1
) {
  for (let x = 0; x < width(a); x += stepX) {
    for (let y = 0; y < height(a); y += stepY) {
      if (isOutOfBounds(a, x, y)) {
        continue;
      }

      callback(get(a, x, y), x, y);
    }
  }
}
