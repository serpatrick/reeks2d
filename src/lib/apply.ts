import forEach from './forEach.js';
import get from './get.js';
import set from './set.js';
import isOutOfBounds from './isOutOfBounds.js';

export default function <T>(
  a: T[][],
  b: T[][],
  x: number,
  y: number,
  callback: (valueA: T, valueB: T) => T
) {
  forEach(b, (valueB, bX, bY) => {
    const aX = x + bX;
    const aY = y + bY;

    if (isOutOfBounds(a, aX, aY)) {
      return;
    }

    const valueA = get(a, aX, aY);
    set(a, aX, aY, callback(valueA, valueB));
  });
}
