import height from './height.js';
import width from './width.js';
import wrap from '../utils/wrap.js';

export default function <T>(a: T[][], x: number, y: number, value: T) {
  x = wrap(x, width(a));
  y = wrap(y, height(a));
  a[y][x] = value;
}
