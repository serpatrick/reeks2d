import { describe, it } from 'mocha';
import { expect } from 'chai';
import wrap from '../../src/utils/wrap.js';

const a = ['a', 'b', 'c'];

describe('utils/wrap', () => {
  it('should return last item if index is 1 lower than min', () => {
    expect(a[wrap(-1, a.length)]).to.equal('c');
  });

  it('should return second to last item if index is 2 lower than min', () => {
    expect(a[wrap(-2, a.length)]).to.equal('b');
  });

  it('should return first item if index is negative length', () => {
    expect(a[wrap(-3, a.length)]).to.equal('a');
  });

  it('should return undefined if index is higher than negative length', () => {
    expect(a[wrap(-4, a.length)]).to.equal(undefined);
  });

  it('should return value if value is between min and max', () => {
    expect(a[wrap(1, a.length)]).to.equal('b');
  });

  it('should return undefined if value is higher than length - 1', () => {
    expect(a[wrap(3, a.length)]).to.equal(undefined);
  });
});
